/*
 Demonstrates how to create a web server using http module

*/

const http = require('http');

	var server = http.createServer();

	server.on('request',function(request, response) {
		console.log('Got a request');
	});

	server.on('listening', function(req,res){
		console.log('Server running');
	});

server.listen(3000);


