/*
 Demonstrates how to create a web server using Express
 Handles three requests, /, /students.json, and /poeple.json
*/

const express = require('express')
const fs = require('fs')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/students.json', (req, res) => fs.readFile('students.json', function(error,content){ 
	if(error) 
		console.log(error);
	res.send(content)}))

app.get('/people.json', (req, res) => fs.readFile('people.json', function(error,content){ 
	if(error) 
		console.log(error);
	res.send(content)}))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))