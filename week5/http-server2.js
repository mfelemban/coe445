/*
 Demonstrates how to create a web server using http module

*/

const http = require('http');
const fs = require('fs');

fs.readFile('index.html',(err, html)=> {
    if(err){
        throw err ;
    }

var server = http.createServer();

    server.on('request',function(request, response) {
        console.log('Got a request');
        response.setHeader('Content-type', 'text/html');
        response.write(html);
        response.write('<p> From Node: Hello world </p>');
        response.end();
    });


        server.on('listening', function(req,res){
        console.log('Server running');
    });


server.listen(3000);


});