/*
 Demonstrates how to create a web server using Express
 Use this file as basis for your homework
*/

const express = require('express')
const fs = require('fs')
var path = require('path')
const app = express()
const port = 3000

let users = {
  1: {
    id: '1',
    name: 'Robin Wieruch',
  },
  2: {
    id: '2',
    name: 'Dave Davids',
  },
  3: {
  	id:'3',
  	name: 'John Smith',
  },
};

let messages = {
  1: {
    id: '1',
    text: 'Hello World',
    userId: '1',
  },
  2: {
    id: '2',
    text: 'By World',
    userId: '2',
  },
};


app.get('/', (req, res) =>  res.sendFile(path.join(__dirname + '/api_tester.html')))

app.get('/users', (req, res) => res.send(Object.values(users)))

app.get('/users/:userId', (req, res) =>  {
	res.send(users[req.params.userId])
});

app.get('/', (req, res) =>  res.sendFile(path.join(__dirname + '/api_tester2.html')))

app.get('/messages', (req, res) => res.send(Object.values(messages)))

app.get('/messages/:userId', (req, res) =>  res.send(messages[req.params.userId]));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))