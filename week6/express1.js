const express = require('express');
const app = express();

app.get('/', function (req,res){
	console.log(req.query.name);
	res.send('Hello World!');
});

app.get('/:name', function (req,res){
	var name = req.params.name;
	console.log(name);
	res.send('Hello World!, ' + name);
});

app.listen(3000, function(){
	console.log("Listening on port 3000");
});