const express = require('express');
const app = express();

app.get('/', function (req,res){
	var name = req.query.name;
	var id = req.query.id;
	res.send(name + ' ' + id);
});

app.listen(3000, function(){
	console.log("Listening on port 3000");
});
