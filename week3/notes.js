// Input/output in JavaScript 

var name = prompt("Please enter your name");
alert("your name is " + name);
console.log("Hello World");

// Boolean Values 

// In JavaScript, the following values are "false"
// •	0
// •	"0" and '0'
// •	 the empty string, '' or ""
// •	undefined
// •	null
// •	NaN

true == "1" //true
0 == false //true 
null == defined //true
null === defined //false
typeof(null) == typeof(undefined)
NaN == NaN //false 

!"hello" //false 
!!"hello" //true
!null  //true
!0 //true
!-1 //false 
!NaN //true 

// High Order Function

// In javaScript, you can pass a function as a parameter to another function

function printMessage(){
	console.log("Hello world");
}
setInterval(printMessage,500);

// or 

setInterval(function(){
	console.log("Hello world");
},500);


// JavaScript Arrays

color = ["red", "yellow", "green", "blue"];

color[0]
color.push("brown");
color.pop();
color.shift("purple");
color.unshift();
color.slice(1,3);
color.indexof("yellow");

for(var i =0;i<color.length;i++)
	console.log(color[i]);

for (var c in color){
	console.log(color[c]);
}

color.forEach(function(c){
console.log(c);
});

var num = [1,2,3,4,5,6];
num.forEach(function(){
	var x = num.pop();
	num.unshift(x*x);
});

var myArray=["a", 1,"this is still an array", undefined, null]

var doubleArray = [1, [2,3,4,5], 6 , [7,8]]

// JavaScript Objects
var car = {make:"toyota", model:"camry",  year:1986, 7:null, "number of passengers":5}
car.make
car.7 // doesn't work 
car["7"] // works
car["number of passengers"] // works
car.number of passengers // doesnot work 
var temp = model // 
car.temp // doesn’t work 
car["temp"] // works



// Array of Objects
var reviews = [{username:"Muhamad", id:"1",comments:["Good", "Bad"]},{username:"Ahmad", id:"2",comments:["Excellent"]},{username:"Ali", id:"1",comments:["Very Good"]}]

// Adding A Method to Object 

var car = {make:"toyota", 
model:"camry",  
year:1986, 
7:null, 
"number of passengers":5,
Info: function(){
	console.log(make + " "  + model + "  " + year);
}}


var car = {make:"toyota", 
model:"camry",  
year:1986, 
7:null, 
"number of passengers":5,
Info: function(make, model, year){
	console.log(make + " "  + model + "  " + year);
}}


var car = {make:"toyota", 
model:"camry",  
year:1986, 
7:null, 
"number of passengers":5,
Info: function(){
	console.log(this.make + " "  + this.model + "  " + this.year);
}}





