var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var fs = require('fs');
const path = require('path');
var fileName = "./students.json";
var file = require(fileName);
var methodOverride = require('method-override')

const DATABASE_NAME = 'COE445';
const DATABASE_URL = 'mongodb://localhost:27017/COE445'

var mongoose = require('mongoose')
mongoose.connect(DATABASE_URL);

var studentSchema = new mongoose.Schema({
	id: String,
	name: String,
	major : String
})

var Student = mongoose.model("Student", studentSchema);


app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride("_method"));
app.use(express.json());

app.listen(3000,function(){
	console.log("Server is running");
});

//RESTful routes
app.get("/",function(req,res){
	// res.sendFile("public/index.html");
	res.redirect("/students");
});

app.get("/students/new", function(req,res){
	res.render("new.ejs");
});

app.post("/students", function(req,res){

	var name = req.body.name;
	var major = req.body.major;
	var id = req.body.id;
	var newStudent = new Student({
		id : id,
		name : name,
		major: major
	})

	// check if there is a student with the same ID 
	Student.find({id:id}, function(err,students){
		if(err) console.log(err);
		else{
			if(students == null ){
				console.log(students)
				newStudent.save(function(err, student){
					if(err) console.log('Something went wrong')
				});
			}
			else{
				
			}
		}
	});
	res.redirect("/students");
});

app.delete("/students/:id", function(req,res){
	var id = req.params.id;
	Student.deleteOne({id:id},function(err,result){
		if(err) console.log(err)
	});
	res.redirect("/students");
});

app.get("/students", function(req,res){
	Student.find({},function(err, students){
		if(err) console.log(err);
		else{
			// res.send(students)
			res.render("students.ejs",{students:students});
		}
	})

});	

app.put("/students/:id",function(req,res){
	var id = req.params.id;
	var name = req.body.name;
	var major = req.body.major;
	Student.update({id:id}, {$set :{ name:name, major:major}});
	res.redirect("/students");
});

app.get("/students/:id/edit", function(req,res){
	var id = req.params.id;
	var cstudent = {};
	cstudent.id = id;

	Student.findOne({id:id}, function(err,result){
		if(err) console.log(err);
		else{
			cstudent.name = result.name;
			cstudent.major = result.major;
			res.render("edit.ejs",{student:cstudent});
		}
	})
	
});

app.get("/students/:id", function(req,res){
	var id = req.params.id;
	Student.findOne({id:id}, function(err, student){
		if(err) console.log(err)
		else{
			console.log(student.name)
			res.render("show.ejs", {name:student.name, major:student.major, id:student.id});
		}
	})
	
});
