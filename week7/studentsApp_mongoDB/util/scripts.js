const fse = require('fs-extra');
const students = require('./visits.json');

// This reformats "dictionary.json" into a JSON file that can be loaded into
// MongoDB via the `mongoimport` command, formatted in the way the dictionary
// examples expect.
(async function() {
  let formatted = '';
  for (const stu in students) {
    const entry = {
      host: students[stu].host,
      name: students[stu].name,
      date: students[stu].date,
      destination: students[stu].destination,
      car_make: students[stu]['car make'],
      car_model: students[stu]['car model'],
      plate_number: students[stu]['plate number'],
      checked_out: students[stu]['checked out']
    }
    console.log(entry)
    formatted += JSON.stringify(entry);
  }
  await fse.writeFile('./formatted-visits.json', formatted);
})();
