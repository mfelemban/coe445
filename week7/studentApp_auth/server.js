var express = require("express");
var app = express();
var bodyParser = require("body-parser");
const path = require('path');
var fileName = "./students.json";
var file = require(fileName);
var methodOverride = require('method-override')

var passport = require('passport');
var LocalStrategy = require('passport-local');
var passportLocalMongoose = require('passport-local-mongoose')
var User = require('./model/user')

const DATABASE_NAME = 'COE445';
const DATABASE_URL = 'mongodb://localhost:27017/COE445'

var mongoose = require('mongoose')
mongoose.connect(DATABASE_URL);

var studentSchema = new mongoose.Schema({
	id: String,
	name: String,
	major : String
})

var Student = mongoose.model("Student", studentSchema);


app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride("_method"));
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session())
app.use(require('express-session')({
	secret:"Use any message here",
	resave : false,
	saveUninitialized:false
})) 

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());



app.listen(3000,function(){
	console.log("Server is running");
});

//RESTful routes

//auth routes
app.get('/register',function(req,res){
	res.render('register.ejs');
})

app.post('/register',function(req,res){
	User.register(new User({username:req.body.username}), req.body.password, function(err,user){
		if(err) {
			console.log(err)
			return res.render('register')
		}
		passport.authenticate("lcoal")(req,res,function(){
			res.redirect('/students');
		})
	})
})

app.get('/login', function(req,res){
	res.render('login.ejs');
})
// middleware
app.post('/login', passport.authenticate("local", {
	successRedirect: '/students',
	failureRedirect: '/login'
}) ,function(req,res){
	
})

app.get('/logout', function(req,res){
	req.logout();
	res.redirect('/')
})


function isLoggedIn(req,res,next){
	console.log('Checking begore deciding')
	if (req.isAuthenticated()) {
		console.log('Yes butt')
		return next()
	}
	res.redirect('/login')
}

app.get("/",function(req,res){
	res.sendFile("public/index.html");
	// res.redirect("/students");
});

app.get("/students/new", function(req,res){
	res.render("new.ejs");
});

app.post("/students", function(req,res){

	var name = req.body.name;
	var major = req.body.major;
	var id = req.body.id;
	var newStudent = new Student({
		id : id,
		name : name,
		major: major
	})

	// check if there is a student with the same ID 
	Student.find({id:id}, function(err,students){
		if(err) console.log(err);
		else{
			if(students == null ){
				console.log(students)
				newStudent.save(function(err, student){
					if(err) console.log('Something went wrong')
				});
			}
			else{
				
			}
		}
	});
	res.redirect("/students");
});

app.delete("/students/:id", function(req,res){
	var id = req.params.id;
	Student.deleteOne({id:id},function(err,result){
		if(err) console.log(err)
	});
	res.redirect("/students");
});

app.get("/students", isLoggedIn,function(req,res){
	Student.find({},function(err, students){
		if(err) console.log(err);
		else{
			// res.send(students)
			res.render("students.ejs",{students:students});
		}
	})

});	

app.put("/students/:id",function(req,res){
	var id = req.params.id;
	var name = req.body.name;
	var major = req.body.major;
	Student.update({id:id}, {$set :{ name:name, major:major}});
	res.redirect("/students");
});

app.get("/students/:id/edit", function(req,res){
	var id = req.params.id;
	var cstudent = {};
	cstudent.id = id;

	Student.findOne({id:id}, function(err,result){
		if(err) console.log(err);
		else{
			cstudent.name = result.name;
			cstudent.major = result.major;
			res.render("edit.ejs",{student:cstudent});
		}
	})
	
});

app.get("/students/:id", function(req,res){
	var id = req.params.id;
	Student.findOne({id:id}, function(err, student){
		if(err) console.log(err)
		else{
			console.log(student.name)
			res.render("show.ejs", {name:student.name, major:student.major, id:student.id});
		}
	})
	
});
