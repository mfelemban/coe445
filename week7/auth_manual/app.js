var express = require('express');
const bcrypt = require('bcrypt')
const users = [];
const app = express();

app.use(express.json());


app.get('/users', function(req,res){
    res.json(users);
})

app.post('/users', async (req,res)=> {
    try{
        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        const user = {name: req.body.name , password: hashedPassword};
        users.push(user);
        console.log(salt)
        console.log(hashedPassword)
    } catch{
        res.status(500).send();
    }
  
    res.status(201).send();
});

// app.post('/users', async (req,res)=> {
//     try{
//         // const salt = await bcrypt.genSalt();
//         // const hashedPassword = await bcrypt.hash(req.body.password, salt);
//         const user = {name: req.body.name , password: req.body.password};
//         users.push(user);
//         console.log(salt)
//         console.log(hashedPassword)
//     } catch{
//         res.status(500).send();
//     }
  
//     res.status(201).send();
// });
 

app.post('/users/login', async (req, res) => {

    const user = users.find(user => user.name = req.body.name);
    if(user == null)
        return res.status(400).send('Cannont find users');
    try{
       if( await bcrypt.compare(req.body.password, user.password)){
           res.send('sucess');
       }
       else{
            res.send('Wrong password');
       }
    }   catch{
        res.status(500).send();
    } 
})
app.listen(3000); 