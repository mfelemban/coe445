var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({extended:true}));
var friends = ["Muhamad", "Yousef" , "Ali" , "Jehad"];


app.get('/', function(req,res){
	// res.send("Hi there, my name is muhamad");
	var temp = "WoW";
	res.render("home.ejs",{name : temp});
});

app.get("/speak/:friend", function(req,res){
	var params = req.params;
	var friend = params.friend;
	res.send("You friend is " + friend);
});

app.get("/friends", function(req,res){
	res.render("friends.ejs",{friends:friends});
});

app.post("/addfriend", function(req,res){
	console.log(req.body);
	var newfriend = req.body.newfriend;
	friends.push(newfriend);
	res.redirect("/friends");
});

app.get("/repeat/:word/:times",function(req,res){
	var message = req.params.word;
	var times = Number(req.params.times);
	var result = "";
	for(i=0;i<=times;i++)
		result += message ;
	res.send(result);
});


app.listen(3000  , function (){
	console.log("Listening on port 3000")
});