var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var fs = require('fs');
const path = require('path');
var fileName = "./students.json";
var file = require(fileName);
var methodOverride = require('method-override')

app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride("_method"));

app.listen(3000,function(){
	console.log("Server is running");
});

//RESTful routes
app.get("/",function(req,res){
	// res.sendFile("public/index.html");
	res.redirect("/students");
});

app.get("/students/new", function(req,res){
	res.render("new.ejs");
});

app.post("/students", function(req,res){
	// create stdudents 
	// redirect 
	var name = req.body.name;
	var major = req.body.major;
	var id = req.body.id;
	if(name != undefined && major != undefined && id !=undefined){
		var stuJSON = fs.readFileSync('./students.json');
		var students = JSON.parse(stuJSON);
		students.push({id: id, name: name, major:major});
		fs.writeFile(fileName, JSON.stringify(students), function(err){
			if(err) return console.log(err);
		});	
	}
	
	res.redirect("/students");
});

app.delete("/students/:id", function(req,res){
	var id = Number(req.params.id);
	var students = JSON.parse(fs.readFileSync('./students.json'));
	for (var i = 0; i < students.length; i++) {
    	var obj = students[i];

	    if (obj.id == id) {
	        students.splice(i, 1);
	    }
	}
	fs.writeFile(fileName, JSON.stringify(students), function(err){
		if(err) return console.log(err);
	});	
	res.redirect("/students");
});
//====================================
// app.get("/students",function(req,res){
// 	res.sendFile(path.join(__dirname+'/students.html'));
// });

// app.get("/students.json",function(req,res){
// 	console.log("sending out JSON file");
// 	res.sendFile(path.join(__dirname+'/students.json'));
// });
//====================================
app.get("/students", function(req,res){
	var stuJSON = fs.readFileSync('./students.json');
	var students = JSON.parse(stuJSON);
	// console.log(students);
	res.render("students.ejs",{students:students});
});	

app.put("/students/:id",function(req,res){
	var id = req.params.id;
	var name = req.body.name;
	var major = req.body.major;
	var students = JSON.parse(fs.readFileSync('./students.json'));
	students.forEach(function(student){
		if(student.id == id){
			student.name = name;
			student.major = major;
		}
	});
	fs.writeFile(fileName, JSON.stringify(students), function(err){
		if(err) return console.log(err);
	});	
	res.redirect("/students");
});

app.get("/students/:id/edit", function(req,res){
	var id = req.params.id;
	var cstudent = {};
	cstudent.id = id;
	var students = JSON.parse(fs.readFileSync('./students.json'));
	students.forEach(function(student){
		if(student.id == id){
			cstudent.name = student.name;
			cstudent.major = student.major;
		}
	});
	res.render("edit.ejs",{student:cstudent});
});

app.get("/students/:id", function(req,res){
	var id = req.params.id;
	var students = JSON.parse(fs.readFileSync('./students.json'));
	var name, major;
	students.forEach(function(student){
		if(student.id == id){
			name = student.name;
			major = student.major;
		}
	});
	res.render("show.ejs", {name:name, major:major, id:id});
});
